import sys
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/FlaskApp/")
sys.path.insert(1, '/var/www/FlaskApp/AmbienteVirtual/lib/python3.6/site-packages')
from app import app as application
if __name__ == "__main__":
    application.run()



