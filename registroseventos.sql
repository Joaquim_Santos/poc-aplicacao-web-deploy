CREATE DATABASE  IF NOT EXISTS `registroseventos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `registroseventos`;
-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: registroseventos
-- ------------------------------------------------------
-- Server version	10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
INSERT INTO `alembic_version` VALUES ('7a80700fae32');
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(200) NOT NULL,
  `Responsavel` varchar(2000) DEFAULT NULL,
  `Local` varchar(2000) DEFAULT NULL,
  `Descricao` varchar(2500) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Hora_inicio` time DEFAULT NULL,
  `Hora_fim` time DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
INSERT INTO `atividade` VALUES (57,'XIII Colóquio Nacional e VI Colóquio Internacional do Museu Pedagógico - “Distopia, barbárie e contraofensivas no mundo contemporâneo”','','Universidade Estadual do Sudoeste da Bahia – Campus de Vitória da Conquista ','','2019-10-15',NULL,NULL),(58,'VI Colóquio Nacional Michel Foucault: da produção de verdades ao governo da vida','','MG | Universidade Federal de Uberlândia ','','2019-09-25',NULL,NULL),(59,'X ENCONTRO DA REDE ESTRADO BRASIL - Autonomia do trabalho docente','','Recife - PE ','','2019-09-19',NULL,NULL),(60,'II Congresso de Estudos da Infância: politizações e estesias','','Universidade do Estado do Rio de Janeiro (UERJ), Faculdade de Educação (Campus Maracanã) ','','2019-09-17',NULL,NULL),(61,'XII Seminário Nacional de Formação dos Profissionais da Educação ','','Faculdade de Educação - FACED/UFBA ','','2019-09-16',NULL,NULL),(62,'VII Seminário Internacional de Políticas Públicas da Educação Básica e Superior e VII Seminário Internacional de Políticas Públicas da Educação Básica e Superior','','Universidade Federal de Santa Maria ','','2019-08-27',NULL,NULL),(63,'CONADE 2019 - 30ª Congresso de Educação do Sudoeste Goiano','','UFG Jataí - Campus Riachuelo - Bloco D ','','2019-08-27',NULL,NULL),(64,'19º Congresso ISATT','','Romênia | Sibiu  ','','2019-07-01',NULL,NULL),(65,'X Seminário Internacional As Redes Educativas e as Tecnologias','','UERJ ','','2019-07-01',NULL,NULL),(66,'X Seminário Internacional As Redes Educativas e as Tecnologias','','UERJ ','','2019-07-01',NULL,NULL),(67,'X Seminário Internacional As Redes Educativas e as Tecnologias','','UERJ ','','2019-07-01',NULL,NULL),(68,'Colóquio Internacional de Educação  Especial e Inclusão Escolar','','Centro de Cultura e Eventos da UFSC R. ENG. AGR. ANDREI CRISTIAN FERREIRA, 570 PANTANAL, FLORIANÓPOLIS – SC  ','','2019-06-25',NULL,NULL),(69,'I Fórum de Educação Especial no Campo e III Encontro Amazônico de Educação Especial ','','Hotel Princesa Louçã - Belém do Pará ','','2019-06-06',NULL,NULL),(70,'Inscrição de Filmes |Mostra Educação | 14ª Mostra de Cinema de Ouro Preto','','Ouro Preto - MG ','','2019-06-05',NULL,NULL),(71,'II Fórum Internacional sobre a Amazônia','','Universidade de Brasília (UnB) ','','2019-06-04',NULL,NULL),(72,'VII Seminário Nacional e III Seminário Internacional Políticas Públicas, Gestão e Práxis Educacional','','BA | Universidade Estadual do Sudoeste da Bahia (UESB) | Campus da Vitório da Conquista ','','2019-05-28',NULL,NULL),(73,'III SEMINÁRIO DE EDUCAÇÃO, CONHECIMENTO E PROCESSOS EDUCATIVOS','','Universidade do Extremo Sul Catarinense - UNESC ','','2019-05-21',NULL,NULL),(74,'5ª edição da Jornada Internacional de Alfabetização - UFRN','','Universidade Federal do Rio Grande do Norte (UFRN) - Natal/RN ( Auditório Oto de Brito Guerra - Reitoria) ','','2019-05-14',NULL,NULL),(75,'XXIX Simpósio de Política e Administração da Educação','','Universidade Federal do Paraná (UFPR) ','','2019-04-16',NULL,NULL),(76,'VII Seminário de Pesquisa e Integração do EHPS','','Pontifícia Universidade Católica de São Paulo - PUC-SP  Rua Ministro Godoy, 969 1º andar  Auditório 117-A ','','2019-03-26',NULL,NULL),(77,'7º Seminário Educação e Formação Humana: Desafios do Tempo Presente e o II Simpósio Educação, Formação e Trabalho','','Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG) – Campus II.  Endereço: Av. Amazonas, 7675 – Nova Gameleira - Belo Horizonte/MG ','','2019-03-23',NULL,NULL),(78,'VI Seminário da Educação Brasileira','','Campinas, SP | CEDES – Unicamp  ','','2018-12-10',NULL,NULL),(79,'Elias Conference ','','Universidade de Saint-Louis, Bruxelas Bélgica ','','2018-12-05',NULL,NULL),(80,'8ª Conferência FORGES | 28 a 30 de novembro | Lisboa - Portugal','','Instituto Politécnico de Lisboa - Portugal ','','2018-11-28',NULL,NULL),(81,'I Congresso Internacional de Pesquisa e Práticas em Educação','','UNESP | Campus de Assis ','','2018-11-28',NULL,NULL),(82,'I Congresso Internacional de Pesquisa e Práticas em Educação','','UNESP | Campus de Assis ','','2018-11-28',NULL,NULL),(83,'6 º GRUPECI','','Centro de Eventos Benedito Nunes - UFPA ','','2018-11-27',NULL,NULL),(84,'VI Colóquio Luso-Brasileiro de Sociologia da Educação','','Instituto de Educação da Universidade do Minho - Braga - Portugal ','','2018-11-26',NULL,NULL);
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'registroseventos'
--

--
-- Dumping routines for database 'registroseventos'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-18 14:22:04
